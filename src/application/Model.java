package application;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Model {
	
	private int typeNumber;
	


	public void WriteFile(String getArticle, String type, String dstLieu, String dptLieu, int nombre, String reference) throws IOException {
	
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
		sdf.setLenient(false);												//Lance ParseException si la cha�ne en entr�e n'est pas au format Date
		Date today = new Date();						    				//R�cup�re la date d'aujourd'hui
		String todayString = sdf.format(today);								//G�n�re une cha�ne de caract�re avec la date d'aujourd'hui au format SimpleDate
		
			switch(type) {
					case "Entr�e":
						typeNumber = 0;
						FileWriter fw;
						fw = new FileWriter(todayString+".csv");
						fw.write(getArticle+";"+typeNumber+";"+dptLieu+";"+nombre+";"+reference);
						fw.close();
						break;
					
					case "Sortie":
						typeNumber = 1;
						fw = new FileWriter(todayString+".csv");
						fw.write(getArticle+";"+typeNumber+";"+dstLieu+";"+dptLieu+";"+nombre+";"+reference);
						fw.close();
						break;
						
					case "Transfert":
						typeNumber = 2;
						fw = new FileWriter(todayString+".csv");
						fw.write(getArticle+";"+typeNumber+";"+dstLieu+";"+dptLieu+";"+nombre+";"+reference);
						fw.close();
						break;
					
		
			}	
	 }
}
