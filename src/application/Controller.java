package application;


import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

public class Controller {
	@FXML
	private TextField article;
	
	@FXML
	private TextField lieuDestination;
	
	@FXML
	private TextField lieuOrigine;
	
	@FXML
	private TextField quantite;
	
	@FXML
	private TextField txtRef;

    @FXML
    private Label dest;

    @FXML
    private Label ori;

    @FXML
    private Label qte;

    @FXML
    private Label ref;
	
	@FXML
	private ChoiceBox<String> type;
	
	@FXML
	private Button valider;
	
	@FXML
	private Button annuler;
	
	@FXML
	private Label message;
	
	@FXML
	private Label erreur;
	
	private String getArticle;
	private String dstLieu;
	private String dptLieu;
	private int nombre;
	private String reference;
	private String getType;
	
	/////////////////////////////////////////////////// Initialisation du stock
	
	private int chaiseLongue = 50;
	private int commode = 30;
	private int table = 60;
	private int tv = 20;
	
	///////////////////////////////////////////////// Getters/Setters
	public int getChaiseLongue() {
		return chaiseLongue;
	}



	public void setChaiseLongue(int chaiseLongue) {
		this.chaiseLongue = chaiseLongue;
	}
	
	public int getCommode() {
		return commode;
	}
	public void setCommode(int commode) {
		this.commode = commode;
	}

	public int getTable() {
		return table;
	}
	
	public void setTable(int table) {
		this.table = table;
	}
	
	public int getTv() {
		return tv;
	}
	
	public void setTv(int tv) {
		this.tv = tv;
	}


	
	public void initialize() {
		type.getItems().add("Entr�e");		//ChoiceBox
		type.getItems().add("Sortie");
		type.getItems().add("Transfert");
		message.setText("Chaise longue : "+chaiseLongue+"\n"+"Commode : "+commode+"\n"+"Table : "+table+"\n"+"TV : "+tv);
		
		lieuDestination.setVisible(false);
		dest.setVisible(false);
		
		lieuOrigine.setVisible(false);
		ori.setVisible(false);
		
		quantite.setVisible(false);
		qte.setVisible(false);
		
		txtRef.setVisible(false);
		ref.setVisible(false);
		
	}
	
	public void display(MouseEvent event) {
		
		getType = type.getValue();
		if (getType != null) {
	        qte.setVisible(true);
	        ref.setVisible(true);
	        quantite.setVisible(true);
        	txtRef.setVisible(true);
        	
	        switch(getType) {
		        case "Entr�e":
	                ori.setVisible(true);
	                lieuOrigine.setVisible(true);
	            	lieuDestination.setVisible(false);
	            	dest.setVisible(false);
	                break;
	                
		        case "Sortie" :
		        	dest.setVisible(true);
		        	lieuDestination.setVisible(true);
		        	lieuOrigine.setVisible(false);
		        	ori.setVisible(false);
		        	break;
		        	
		        case "Transfert" :
		        	ori.setVisible(true);
		        	dest.setVisible(true);
		        	lieuOrigine.setVisible(true);
		        	lieuDestination.setVisible(true);
	            }
			}
	}
	
	
	public void validation(ActionEvent e) {
	
	try {
		Model model = this.init();
	
		getArticle = article.getText();
		dstLieu = lieuDestination.getText();
		dptLieu = lieuOrigine.getText();
		reference = txtRef.getText();
		getType = type.getValue();
		
		this.checkForm();
		model.WriteFile(getArticle, getType, dstLieu, dptLieu, Integer.parseInt(quantite.getText()),reference );
			
		
	  } catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (Exception e2) {
			System.out.println(e2.getMessage());
		}
  }

	private Model init() {
		 erreur.setText("");
		 Model model = new Model();	
		 return model;
	 }

	private void checkForm() throws Exception {
		if(getArticle.isEmpty()) {			
			throw new Exception("le champ article est vide");
		 }
		if(dstLieu.isEmpty()) {			
			throw new Exception("le champ Lieu de stock de destination est vide");
		 }
		if(dptLieu.isEmpty()) {			
			throw new Exception("le champ Lieu de stock d'origine est vide");
		 }
		if(getArticle.isEmpty()) {			
			throw new Exception("le champ article est vide");
		 }
		try {
			Integer.parseInt(quantite.getText());	
		}catch(Exception e){
			throw new Exception("Le champ quantit� doit �tre num�rique"); 	
		}
			
			
	}
		
	
}